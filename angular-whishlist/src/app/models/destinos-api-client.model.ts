import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {} from '@ngrx/store-devtools';

import {
  ElegidoFavoritoAction,
  NuevoDestinoAction,
} from './destinos-viajes-state.model';
import { AppState } from './../app.module';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

  constructor(private store: Store<AppState>) {
    this.store
      .select((state) => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store.subscribe((data) => {
      console.log('all store');
      console.log(data);
    });
  }

  add(d: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter(function (d) {
      return d.id.toString() === id;
    })[0];
  }

  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
